export default [
  {
    path: '/component',
    component: () =>
      import(/* webpackChunkName: "component" */ '@/modules/component'),
  },
];
