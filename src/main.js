import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';

import ButtonCpn from '@/components/general/Button';

Vue.config.productionTip = false;

Vue.component('ButtonCpn', ButtonCpn);

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
